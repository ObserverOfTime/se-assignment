# Generate pdf using xelatex
$pdf_mode = 5;

# Define xelatex command
$xelatex = "xelatex -interaction=nonstopmode -shell-escape -synctex=1 %O %P";

# Define previewer command
$pdf_previewer = "pympress -N bottom %O %S";

# Delete generated files
$cleanup_includes_generated = 1;
$bibtex_use = 2;

# Define output directory
$aux_dir = "build/";
$out_dir = "build/";

# Define the job name
$jobname = $ENV{'MODE'} || "presentation";

# Include extra code
$pre_tex_code = "\\PassOptionsToClass{$jobname}{beamer}";
$pre_tex_code .= $jobname eq "handout" ?
    q[\AtBeginDocument{\setbeameroption{hide notes}}] :
    q[\AtBeginDocument{\setbeameroption{show notes on second screen=bottom}}];

# Define the default files
@default_files = ("softeng.tex");
