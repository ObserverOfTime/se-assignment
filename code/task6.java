Map<String, Integer> f(List<Product> products) {
    return products.stream().collect(
        toMap(Product::getDepartment,
            Product::getPrice,
            minBy(Integer::compareTo)
        )
    );
}

Map<String, Integer> f(List<Product> products) {
    Map<String, List<Product>> groups =
        new HashMap<>();
    for (Product p : products) {
        String department = p.getDepartment();
        if (groups.containsKey(department)) {
            groups.get(department).add(p);
        } else {
            List<Product> group = new ArrayList<>();
            group.add(p);
            groups.put(department, group);
        }
    }
    Map<String, Integer> computed = new HashMap<>();
    for (Map.Entry<String, List<Product>> group :
            groups.entrySet()) {
        Integer c = null;
        List<Product> departmentProducts =
            group.getValue();
        for (Product product : departmentProducts) {
            if (c == null || product.getPrice() < c) {
                c = product.getPrice();
            }
        }
        computed.put(group.getKey(), c);
    }
    return computed;
}
