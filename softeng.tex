\documentclass[10pt]{beamer}

% Packages
\usepackage[english,greek]{babel}
\usepackage[
    style=alphabetic,
    language=auto,
    autolang=none,
    backend=biber,
    sorting=anyt,
    maxnames=5,
    minnames=3]{biblatex}
\usepackage[
  cachedir=minted-cache,
  outputdir=build]{minted}
\usepackage[autostyle]{csquotes}
\usepackage{copyrightbox}
\usepackage{fontspec}
\usepackage{moresize}
\usepackage{tikz}

% Metadata
\title{The effect of Java APIs for declarative processing on program understandability}
\author{Ιωάννης Σομός}
\institute[]{Οικονομικό Πανεπιστήμιο Αθηνών}
\date{\today}

% Colors
\definecolor{auebred}{HTML}{762124}
\definecolor{mintedbg}{HTML}{F0F0F0}
\definecolor{java}{HTML}{E11F21}
\definecolor{kotlin}{HTML}{F88A02}
\definecolor{scala}{HTML}{DF311E}

% Font options
\setmainfont{Libertinus Serif}
\setsansfont[Scale=1.1]{Libertinus Sans}
\setmonofont[RawFeature={+ss01,-calt}]{Fantasque Sans Mono}
\newfontfamily{\nerdfont}{Symbols Nerd Font}

% Bibliography options
\nocite{*}
\addbibresource{cite.bib}
\DeclareFieldFormat[software]{titleaddon}{\texttt{\ssmall #1}}

% Beamer options
\usetheme{default}
\usecolortheme{beaver}
\titlegraphic{\includegraphics[width=0.5\textwidth]{AUEB}}

\setbeamertemplate{headline}{}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{enumerate items}{\insertenumlabel.}
\setbeamertemplate{itemize item}{
  \raisebox{0.25ex}{\footnotesize\char"2666}}
\setbeamertemplate{itemize subitem}{
  \raisebox{0.25ex}{\footnotesize\char"25C7}}
\setbeamertemplate{section in toc}{
  \color{auebred}{\raisebox{0.3ex}{\small\java}}
  ~\color{black}{\inserttocsection}}
\setbeamertemplate{note page}{
  \insertvrule{\paperheight}{note page.bg}
  \vskip-\paperheight
  \usebeamercolor[fg]{note page}
  \insertnote}

\setbeamerfont{frametitle}{family=\rmfamily}
\setbeamerfont{title}{size=\LARGE, family=\rmfamily}
\setbeamerfont{author}{size=\Large, family=\rmfamily}
\setbeamerfont{institute}{size=\large, family=\rmfamily}
\setbeamerfont{date}{size=\normalfont, family=\rmfamily}
\setbeamerfont{description item}{series=\bfseries}

\setbeamercolor{frametitle}{bg=auebred}
\setbeamercolor{titlelike}{bg=auebred, fg=white}
\setbeamercolor{itemize item}{fg=auebred}
\setbeamercolor{itemize subitem}{fg=auebred}
\setbeamercolor{description item}{fg=auebred}
\setbeamercolor{enumerate item}{fg=auebred}
\setbeamercolor{palette tertiary}{bg=auebred}
\setbeamercolor{bibliography entry author}{fg=black}
\setbeamercolor{bibliography entry note}{fg=black}

\setbeamersize{description width=1em}

% Other options
\urlstyle{same}
\hypersetup{
    colorlinks=true, linkcolor=.,
    citecolor=teal, urlcolor=cyan}
\graphicspath{{images/}}
\setminted{
    breaklines=true, autogobble=true, numbersep=6pt,
    fontsize=\footnotesize, frame=single, framesep=1ex,
    labelposition=bottomline, style=friendly, bgcolor=mintedbg}

% Commands
\newcommand{\copyrightimage}[2][]{
  \centering\copyrightbox[b]{#2}{
    \centering\rmfamily\tiny{#1}}}

\newcommand{\citefnt}[2][]{
  \begingroup
  \let\thefootnote\relax
  \footnotetext{\ssmall\cite[#1]{#2}}
  \endgroup}

\newcommand{\lbl}[1]{\tiny\textsf{#1}}
\newcommand{\icon}[1]{%
  \ssmall{\color{#1}{\csname #1\endcsname}}
  {\color{black}{\MakeUppercase #1}}}

\newcommand{\java}{\nerdfont\symbol{"E738}}
\newcommand{\scala}{\nerdfont\symbol{"E737}}
\newcommand{\kotlin}{\nerdfont\symbol{"E634}}

\newcommand{\cmark}{
  \tikz[scale=0.25, color=green!70!black]{
      \draw[line width=0.7, line cap=round] (0.25, 0) to [bend left=9] (1, 1);
      \draw[line width=0.8, line cap=round] (0, 0.35) to [bend right=1] (0.25, 0);
  }}
\newcommand{\xmark}{
  \tikz[scale=0.25, color=red!75!black]{
      \draw[line width=0.7, line cap=round] (0, 0)      to [bend left=6]  (1, 1);
      \draw[line width=0.7, line cap=round] (0.2, 0.95) to [bend right=3] (0.8, 0.05);
  }}

\renewcommand{\subtitlepunct}{\addcolon\addspace}

\renewcommand{\bibfont}{\scriptsize}

\begin{document}

\frame{\titlepage}
\note{
  Φτάσαμε, λοιπόν, στην τελευταία παρουσίαση όπου θα μιλήσουμε για ένα θέμα
  που θα φανεί χρήσιμο σε όσους έχουν την ατυχία να εργαστούν σε Java. Το θέμα
  αυτό είναι η επίδραση των declarative API της στη σαφήνεια ενός προγράμματος.
}

\begin{frame}{Επισκόπηση}
  \NoHyper
  \tableofcontents
  \endNoHyper
  \note{
    Συγκεκριμένα, θα μάθουμε για lambdas και streams,
    θα δούμε γιατί και πότε χρησιμοποιούνται τα lambdas,
    θα συγκρίνουμε υλοποιήσεις με και χωρίς streams, θα
    μιλήσουμε λίγο για το reactive programming, και θα
    κάνουμε μια αναφορά σε αντίστοιχα API όμοιων γλωσσών.
  }
\end{frame}

\section{Τι είναι τα lambdas;}
\begin{frame}{\secname}
  \begin{itemize}
    \item Αντικείμενα που υλοποιούν κάποιο functional interface\pause
    \begin{itemize}
      \item \texttt{Runnable} \textit{\small (1.0)}
      \item \texttt{Comparator} \textit{\small (1.2)}
      \item \texttt{Callable} \textit{\small (1.5)}
      \item \texttt{Function} \textit{\small (1.8)}
    \end{itemize}\pause
    \item Στο bytecode διαχειρίζονται σαν συναρτήσεις\pause
    \item Οι τύποι παραμέτρων και επιστροφής παραλείπονται
  \end{itemize}
  \pause
  \begin{columns}[T]
    \begin{column}{0.475\textwidth}
      \inputminted[label=\lbl{Anonymous class}, lastline=6,
                    fontsize=\ssmall]{java}{code/lambda.java}
    \end{column}
    \begin{column}{0.525\textwidth}
      \inputminted[label=\lbl{Lambda}, firstline=8,
                   fontsize=\ssmall]{java}{code/lambda.java}
    \end{column}
  \end{columns}
  \citefnt{neward2013java}
  \note<1>{
    Τι είναι λοιπόν τα lambdas; Ουσιαστικά, είναι
    αντικείμενα που υλοποιούν κάποιο functional interface,
    όπως τα λέει η Java, και προστέθηκαν στην έκδοση 8,
    το 2014, μαζί με τα streams που θα δούμε σε λίγο.
  }
  \note<2>{
    Μερικά functional interfaces είναι το \texttt{Runnable} (από τη
    Java 1.0) που δεν δέχεται παραμέτρους και δεν επιστρέφει αποτέλεσμα.
    Το \texttt{Comparator} (από τη Java 1.2) που δέχεται δύο παραμέτρους
    ίδιου τύπου και επιστρέφει έναν αριθμό ανάλογα με το αν είναι ίσες ή
    όχι. Το \texttt{Callable} (από τη Java 1.5) που δεν δέχεται παραμέτρους
    αλλά επιστρέφει αποτέλεσμα οποιουδήποτε τύπου, και το \texttt{Function}
    που προστέθηκε, μεταξύ άλλων, στη Java 1.8, δέχεται μία παράμετρο
    οποιουδήποτε τύπου και επιστρέφει αποτέλεσμα οποιουδήποτε τύπου. Αξίζει
    να σημειωθεί ότι σε Java δεν υπάρχει functional interface γενικής χρήσης
    που να δέχεται περισσότερες από 2 παραμέτρους, ενώ η Scala και η Kotlin,
    για παράδειγμα, υποστηρίζουν μέχρι 22. Φυσικά, μπορούμε να ορίσουμε και
    δικό μας functional interface με περισσότερες παραμέτρους αν το χρειαζόμαστε.
  }
  \note<3>{
    Είπαμε ότι είναι αντικείμενα, όμως στο bytecode δεν δημιουργούνται πράγματι
    αντικείμενα, αλλά μεταφράζονται σε συναρτήσεις, οπότε έχουν λιγότερο overhead.
    Επίσης, έχουν απευθείας πρόσβαση στα πεδία και τις μεθόδους της κλάσης, ενώ
    οι ανώνυμες κλάσεις έχουν ξεχωριστό reference οπότε πρέπει να χρησιμοποιήσουν
    και το όνομα της εξωτερικής κλάσης για να καλέσουν το δικό της \texttt{this}.
  }
  \note<4>{
    Επιπλέον, σε αντίθεση με τις κανονικές συναρτήσεις, δεν χρειάζεται συνήθως να
    ορίσουμε τύπους παραμέτρων ή επιστροφής. Αυτό γιατί στο bytecode χρησιμοποιείται
    το opcode \texttt{invokedynamic}, το οποίο καταλαβαίνει αυτόματα τους τύπους.
    Υπάρχουν βέβαια περιπτώσεις που πρέπει να τους ορίσουμε, αν υπάρχει ασάφεια.
  }
  \note<5>{
    Κι εδώ είναι ένα παράδειγμα ανώνυμης κλάσης και
    του αντίστοιχου lambda με τις μισές γραμμές κώδικα.
  }
\end{frame}

\section{Πως λειτουργούν τα streams;}
\begin{frame}{\secname}
  \begin{overlayarea}{\textwidth}{0.775\textheight}
  \begin{columns}[T]
    \begin{column}{0.575\textwidth}
      \textbf{Stream pipeline} \cite{java8stream}
      \begin{enumerate}[i]
        \item Source
          \begin{itemize}
            \item Potentially infinite
          \end{itemize}\pause
        \item Intermediate operations
          \begin{itemize}
            \item Always lazy
            \item Stateless or stateful
            \item Can short-circuit
          \end{itemize}\pause
        \item Terminal operation
        \begin{itemize}
          \item Usually eager
          \item Can short-circuit
        \end{itemize}
      \end{enumerate}
      \pause
      \alt<6|handout:0>{
        \inputminted[firstline=6]{java}{code/widgets.java}
      }{
        \inputminted[lastline=4]{java}{code/widgets.java}
      }
    \end{column}
    \pause
    \begin{column}{0.425\textwidth}
      \alt<6|handout:0>{
        \includegraphics[width=\columnwidth]{streams2.drawio}
      }{
        \includegraphics[width=\columnwidth]{streams.drawio}
      }
    \end{column}
  \end{columns}
  \end{overlayarea}
  \note<1>{
    Τα streams, ή ροές, ξεκινάνε από μία πηγή, όπως κάποια
    συλλογή, γεννήτρια συνάρτηση, αρχείο, ή βάση δεδομένων.
    Ορισμένες πηγές μπορεί να παράγουν απεριόριστα δεδομένα.
  }
  \note<2>{
    Μετά, ακολουθούν μηδέν ή περισσότερες ενδιάμεσες διαδικασίες.
    Αυτές είναι πάντα lazy, δηλαδή δεν εκτελούνται ακόμα, και
    χωρίζονται σε stateless (όπως η \texttt{filter}) και stateful
    (όπως η \texttt{sorted}). Ορισμένες μπορούν να κάνουν
    short-circuit (να βραχυκυκλώσουν) το stream και να επιστρέψουν
    πριν επεξεργαστούν όλα τα στοιχεία (όπως η \texttt{limit}).
    Νομίζω ότι είναι κατανοητό τι κάνει η κάθε μέθοδος από το όνομά της.
  }
  \note<3>{
    Τέλος, υπάρχει μία τερματική διαδικασία (όπως η \texttt{sum}).
    Οι τερματικές διαδικασίες είναι σχεδόν πάντα eager, δηλαδή
    εκτελούν άμεσα όλες τις διαδικασίες του pipeline, με εξαίρεση
    τις \texttt{iterator} και \texttt{spliterator} οι οποίες
    μετατρέπουν το stream σε άλλη lazy δομή. Υπάρχουν κι εδώ
    διαδικασίες που κάνουν short-circuit (όπως η \texttt{findFirst}).
  }
  \note<4>{
    Αυτό το παράδειγμα είναι από το documentation. Η πηγή είναι μία
    συλλογή με widgets, και το αποτέλεσμα είναι το άθροισμα των βαρών των
    widget με κόκκινο χρώμα. Φαίνεται ότι περνάνε όλα τα αντικείμενα στο
    \texttt{filter}, μετά στο \texttt{mapToInt} και τέλος στο \texttt{sum}.
  }
  \note<5>{
    Στην πραγματικότητα, όμως, οι ενδιάμεσες διαδικασίες εκτελούνται σε
    κάθε αντικείμενο ξεχωριστά και συλλέγονται στην τερματική. Αυτό καθιστά
    πολύ εύκολη την εκτέλεση του pipeline παράλληλα, χρησιμοποιώντας τη
    μέθοδο \texttt{parallelStream} αντί για \texttt{stream} αν έχουμε
    συλλογή ή την \texttt{parallel} αν έχουμε ήδη κάποιο σειριακό stream.
  }
  \note<6>{
    Βέβαια, αν είχαμε και κάποια stateful διαδικασία όπως τη \texttt{sorted}
    εδώ, αυτή θα χώριζε ουσιαστικά το pipeline σε δύο μέρη, καθώς θα
    συγκέντρωνε τα αντικείμενα και θα τα μοίραζε ξανά όταν ολοκληρωνόταν.
  }
\end{frame}

\section{Χρήση lambdas από προγραμματιστές}
\begin{frame}{\secname}
  \begin{columns}[T]
    \small
    \begin{column}{0.55\textwidth}
      \textbf{Λόγοι χρήσης} \cite{mazinanian2017understanding}
      \begin{enumerate}
        \footnotesize
        \item Terseness of lambdas
        \item Avoiding the implementation\\ of new classes
        \item Behavior parameterization\\ and reusability
        \item API convention enforcement
        \item Migration to Java 8
        \item Implementing listeners and callbacks
        \item Lazy evaluation
        \item Maintaining consistency
        \item Object-oriented design patterns
        \item Performance
        \item Simplify testing
        \item Favor composition over inheritance
        \item Multi-threading
        \item Exception handling
      \end{enumerate}
    \end{column}
    \pause
    \begin{column}{0.45\textwidth}
      \textbf{Περιπτώσεις χρήσης} \cite{nielebock2019programmers}
      \centering
      \includegraphics[angle=270, origin=c, height=0.6\textheight]{classification}
    \end{column}
  \end{columns}
  \note<1>{
    Εδώ βλέπουμε διάφορους λόγους χρήσης των lambdas σύμφωνα με μία έρευνα
    του '17 πάνω σε ορισμένα project ανοιχτού κώδικα. Ο κυριότερος λόγος
    ήταν το μικρό μέγεθος του κώδικα σε σχέση με τις ανώνυμες ή επώνυμες
    κλάσεις. Μετά, η αποφυγή υλοποίησης νέων κλάσεων, η παραμετροποίηση
    αλγορίθμων (π.χ., ταξινόμησης) με επαναχρησιμοποιήσιμο κώδικα, η επιβολή
    των συμβάσεων κάποιου API, η μετατροπή του project σε Java 8, η υλοποίηση
    listeners και callbacks σε UI ή request-response, η αναβολή της αποτίμησης
    κάποιου υπολογισμού μέχρι να μπορεί ή να χρειαστεί να εκτελεστεί, η διατήρηση
    της συνοχής όταν χρησιμοποιούνται ήδη, η διευκόλυνση στην υλοποίηση κάποιων
    σχεδιαστικών προτύπων (όπως του Visitor Pattern που χρησιμοποιείται για
    παράδειγμα σε μεταγλωττιστές), η επίδοση (καθώς είπαμε ότι έχουν λιγότερο
    overhead), η απλούστευση του κώδικα των test (κάτι που κάναμε κι εμείς στο
    δεύτερο παραδοτέο τρέχοντας transactions με lambda ώστε να μην τα ανοιγοκλείνουμε
    σε κάθε test), η σύσταση απλής πολυμορφικής συμπεριφοράς χωρίς πολλές επεκτάσεις
    κλάσεων, η παραλληλοποίηση διεργασιών και, τέλος, η διαχείριση εξαιρέσεων.
  }
  \note<2>{
    Μία άλλη έρευνα του '19 μελέτησε τις περιπτώσεις χρήσης των lambdas
    πάλι σε διάφορα project ανοιχτού κώδικα. Οι λευκές μπάρες είναι αρχεία
    που δεν περιέχουν lambdas ενώ οι γκρι είναι αρχεία που περιέχουν.
    Βλέπουμε ότι χρησιμοποιούνται περισσότερο από τις εναλλακτικές
    και σε μεγάλο ποσοστό για test, υλοποίηση αλγορίθμων και UI.
  }
\end{frame}

\section{Σύγκριση υλοποιήσεων με ή χωρίς το stream API}
\begin{frame}{\secname~(1)}
  \begin{columns}[T]
    \begin{column}{0.475\textwidth}
      \inputminted[label=\lbl{Stream}, lastline=8,
                   fontsize=\tiny]{java}{code/task6.java}
      \visible<3- >{
        \copyrightimage[\copyright~Something of that Ilk]{
          \includegraphics[height=0.4\textheight]{java}}}
    \end{column}
    \begin{column}{0.525\textwidth}
      \visible<2- >{
        \inputminted[label=\lbl{Loop}, firstline=10,
                     fontsize=\tiny]{java}{code/task6.java}}
    \end{column}
  \end{columns}
  \citefnt[\S3]{mehlhorn2022imperative}
  \note<1>{
    Εδώ βλέπουμε μία μέθοδο υλοποιημένη με stream η οποία μετατρέπει
    μία λίστα από προϊόντα σε map με κλειδί το τμήμα του προϊόντος
    και τιμή την χαμηλότερη τιμή των προϊόντων για αυτό το τμήμα.
  }
  \note<2-3>{
    Και η αντίστοιχη υλοποίηση χωρίς stream. Μία καλή περιγραφή αυτής
    της υλοποίησης υπάρχει σε αυτό το panel από ένα comic του 2011.
  }
\end{frame}

\begin{frame}{\secname~(2)}
  Σαφήνεια υλοποιήσεων με streams ή loops
  \cite[\S4]{mehlhorn2022imperative}:
  \begin{itemize}
    \item 78\% μεγαλύτερος χρόνος απόκρισης με loops
    \item 3 φορές λιγότερες λάθος απαντήσεις με streams
  \end{itemize}
  \vfill\pause
  \begin{columns}[T, onlytextwidth]
    \begin{column}{0.48\textwidth}
      \includegraphics[width=\columnwidth]{speed}
    \end{column}
    \pause
    \begin{column}{0.515\textwidth}
      \includegraphics[width=\columnwidth]{rating}
    \end{column}
  \end{columns}
  \note<1>{
    Το comic αυτό μπορεί να είναι παλιό και σατιρικό, αλλά το
    παράδειγμα ήταν από μία φετινή έρευνα η οποία κατέληξε στο ίδιο
    συμπέρασμα. Έδωσαν 7 ζευγάρια παραδειγμάτων σε 20 προγραμματιστές
    οι οποίοι είχαν κάποια εμπειρία με τα streams, αλλά τους έκαναν
    και μία εισαγωγή σε αυτά. Η μία υλοποίηση του κάθε παραδείγματος
    χρησιμοποιούσε stream ενώ η άλλη loop, και τους ρώτησαν για
    κάθε υλοποίηση αν καταλαβαίνουν τι κάνει, μετρώντας τους χρόνους
    απάντησης. Οι ερωτηθέντες χρειάστηκαν σχεδόν 80\% περισσότερο χρόνο
    για να αντιληφθούν τη λειτουργία των παραδειγμάτων χωρίς τη χρήση
    stream και έδωσαν μάλιστα 30 λάθος απαντήσεις στις υλοποιήσεις
    με χρήση loop ενώ μόνο 10 στις υλοποιήσεις με χρήση stream.
  }
  \note<2>{
    Αυτό το γράφημα απεικονίζει τους χρόνους απάντησης. Το παράδειγμα
    που είδαμε προηγουμένως είναι το έκτο που ήταν και το δυσκολότερο
    στην κατανόηση. Για τις λάθος απαντήσεις δεν υπήρχε κάποιο γράφημα.
  }
  \note<3>{
    Τους ζητήθηκε, επίσης, να δώσουνε μία προσωπική
    βαθμολογία στην απόδοση των υλοποιήσεων, τον
    χρόνο που χρειάστηκαν για να τις καταλάβουν
    και το πόσο εμφανίσιμες και ευανάγνωστες ήταν.
    Παρατηρούμε ότι τα streams υπερτερούν των loops.
  }
\end{frame}

\section{Reactive Programming και Observer Pattern}
\begin{frame}{\secname}
  Το reactive programming έναντι του object-oriented observer pattern
  σε Scala \cite[\S5.2.3--5.2.4]{salvaneschi2017positive}:\pause
  \linebreak
  \begin{itemize}
    \item[\cmark] Reduced Boilerplate Code\pause
    \item[\cmark] Better Readability\pause
    \item[\cmark] Automatic Consistency of Reactive Values\pause
    \item[\cmark] Shorter Code\pause
    \item[\cmark] Declarative Nature\pause
    \item[\cmark] Ease of Composition\pause
    \item[\cmark] Separation of Concerns\pause
    \item[\xmark] Learning Curve\pause
    \item[\xmark] Level of Abstraction\pause
    \item[\xmark] Relation with Functional Programming
  \end{itemize}
  \note<1>{
    Εκτός από τη Java, έχει γίνει έρευνα και για τα πλεονεκτήματα ή μειονεκτήματα
    του reactive programming έναντι του object-oriented observer pattern σε Scala.
    Το observer pattern είναι ένα σχεδιαστικό πρότυπο στο οποίο πολλά αντικείμενα
    (observers) ειδοποιούνται για αλλαγές σε ένα αντικείμενο (observable) με
    αντικειμενοστραφή τρόπο. Το reactive programming βασίζεται σε αυτό το πρότυπο
    αλλά αντί για observers χρησιμοποιεί ασύγχρονες ροές δεδομένων με δηλωτικό τρόπο.
  }
  \note<2>{
    Πρώτο πλεονέκτημα είναι η μείωση του boilerplate κώδικα (δηλαδή
    του κώδικα που δεν έχει να κάνει με την ίδια την υλοποίηση).
  }
  \note<3>{
    Περισσότερη σαφήνεια λόγω αυτής της μείωσης μεταξύ και άλλων παραγόντων.
  }
  \note<4>{
    Αυτόματη συνέπεια των τιμών καθώς περνάνε μέσα από
    μία συνεχή ροή και όχι από ξεχωριστά αντικείμενα.
  }
  \note<5>{
    Λιγότερος κώδικας υλοποίησης ακόμη κι αν εξαιρέσουμε το boilerplate.
  }
  \note<6>{
    Η ίδια η δηλωτική φύση του κώδικα θεωρήθηκε κάτι θετικό.
  }
  \note<7>{
    Διευκόλυνση της σύνθεσης με κομψότερο και εύληπτο συντακτικό.
  }
  \note<8>{
    Διαχωρισμός των ενδιαφερόντων της διάδοσης των αλλαγών
    και της υπόλοιπης λογικής του προγράμματος.
  }
  \note<9>{
    Από την άλλη, ένα αρνητικό είναι ότι έχει μεγαλύτερη καμπύλη εκμάθησης.
  }
  \note<10>{
    Επίσης, η αφαιρετικότητα αυτής της τεχνικής καθιστά
    δύσκολο να αντιληφθούμε πως λειτουργεί από πίσω.
  }
  \note<11>{
    Τέλος, άτομα λιγότερο εξοικειωμένα με τον συναρτησιακό
    προγραμματισμό δεν είναι εύκολο να αντιληφθούν αυτή τη λογική.
  }
\end{frame}

\section{Αντίστοιχα API σε άλλες γλώσσες}
\begin{frame}<handout:1-3>{\secname}
  \alt<3-|handout:2-3>{
    \mint[label=\icon{java}]{java}{numbers.stream().map(i -> i * i).filter(i -> i > 100).findFirst()}
  }{
    \mint[label=\icon{java}]{java}{numbers.stream().map(i -> i * i).toList()}
  }
  \pause
  \alt<2|handout:1>{
    \mint[label=\icon{kotlin}]{kotlin}{numbers.map { it * it }}
    \mint[label=\icon{scala}]{scala}{numbers.map(i => i * i)}
  }{
    \invisible<3|handout:1>{
      \alt<5|handout:3>{
        \mint[label=\icon{kotlin}]{kotlin}{numbers.asSequence().map { it * it }.find { it > 100 }}
        \mint[label=\icon{scala}]{scala}{numbers.view.map(i => i * i).find(_ > 100)}
      }{
        \mint[label=\icon{kotlin}]{kotlin}{numbers.map { it * it }.find { it > 100 }}
        \mint[label=\icon{scala}]{scala}{numbers.map(i => i * i).find(_ > 100)}
      }
    }
  }
  \note<1|handout:1>{
    Μια που αναφέραμε τη Scala, ας δούμε το δικό της declarative API,
    καθώς και της Kotlin, αφού και οι δύο χρησιμοποιούν το JVM. Έστω
    ότι έχουμε μια λίστα από αριθμούς και θέλουμε τα τετράγωνα αυτών σε
    μια νέα λίστα. Με το stream API σε Java, μπορεί να γίνει κάπως έτσι.
  }
  \note<2|handout:1>{
    Σε Kotlin και Scala, αυτό είναι αρκετά πιο απλό. Δεν χρειάζεται η
    μετατροπή της λίστας σε stream γιατί αντίστοιχες μέθοδοι με αυτές
    των streams, και άλλες που δεν υπάρχουν σε Java, έχουν οριστεί για
    κάθε κλάση που υλοποιεί κάποια συλλογή. Όμως, δεν είναι lazy.
  }
  \note<3|handout:2>{
    Ας πούμε ότι δεν θέλουμε όλη τη λίστα των τετραγώνων, αλλά μόνο
    το πρώτο που υπερβαίνει το 100. Όπως είπαμε νωρίτερα, αυτός ο
    κώδικας θα σταματήσει αμέσως την επεξεργασία μόλις το βρει.
  }
  \note<4|handout:2>{
    Το ίδιο δεν ισχύει σε Kotlin και Scala. Θα εκτελεστεί πρώτα το map σε
    όλη τη λίστα, και μετά θα τρέξει το find το οποίο θα σταματήσει μόλις
    βρεθεί αριθμός μεγαλύτερος του 100. Αυτό συμβαίνει γιατί, στις περισσότερες
    περιπτώσεις, οι ενδιάμεσες διαδικασίες είναι από πίσω υλοποιημένες με μία απλή
    επανάληψη οπότε είναι eager (όπως και οι επαναλήψεις πάνω σε συλλογές στη Java).
  }
  \note<5|handout:3>{
    Εδώ χρειάζεται να μετατραπεί η λίστα σε κάποια άλλη lazy δομή η οποία γίνεται
    short-circuit όπως και τα streams σε Java, με τη μέθοδο \texttt{asSequence}
    σε Kotlin ή \texttt{view} σε Scala. Μην ανησυχείτε, αυτά είναι εκτός ύλης.
  }
\end{frame}

% TODO: I should, but I won't
% \section{Παρατηρήσεις και συμπεράσματα}
% \begin{frame}{\secname}
%   \begin{itemize}
%   \end{itemize}
% \end{frame}

\begin{frame}{Βιβλιογραφία}
  \parbox{0.96\textwidth}{
    \selectlanguage{english}
    \hyphenpenalty=99999
    \printbibliography[heading=none]}
\end{frame}

\end{document}
