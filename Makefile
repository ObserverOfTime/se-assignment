LATEXMK := latexmk
PDFPC := pdfpc

.DEFAULT_GOAL := build/presentation.pdf

all: build/handout.pdf build/presentation.pdf

build/%.pdf: softeng.tex
	@MODE=$(basename $(@F)) $(LATEXMK) $< \
		$(if $(findstring B,$(firstword -$(MAKEFLAGS))),-g,)

preview: softeng.tex
	@$(LATEXMK) $< -pvc

present: build/presentation.pdf
	@$(PDFPC) -d 45 -l 10 -f plain -n bottom $<

.PHONY:
clean:
	@$(LATEXMK) $(if $(FORCE),-C,-c)
